Api::Application.routes.draw do
  resources :users, only: [:index, :show, :create, :update, :destroy]

  resources :users, only: [:index] do
    resources :contacts#, only: [:index]
  end

  resources :contact_shares, only: [:index, :show, :create, :update, :destroy]
end
