# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Api::Application.config.secret_key_base = '5fa7efc841833dff0092d3de57da50bacf4fcfd0a696863a0118a341cbbde5cf1f88a01a1a8ae84874e12bb7c5ef1bb798207989a068ecc3b3ff5320f804c8c1'
