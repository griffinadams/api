class ContactShareController < ApplicationController
  def create
    contact_share = ContactShare.new(share_params)
    if contact_share.save!
      render json: contact_share
    end
  end

  def destroy
    contact_share = ContactShare.find(params[:id])
    if contact_share.destroy
      render json: ContactShare.all
    end
  end



  private
  def share_params
    params.require(:contact_share).permit(:contact_id, :user_id)
  end
end
