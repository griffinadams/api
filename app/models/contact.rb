class Contact < ActiveRecord::Base
  validates :name, :email, presence: true
  validates :email, uniqueness: { scope: :user_id }

  belongs_to :owner,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id

  has_many :shared_contacts,
    class_name: "ContactShare",
    foreign_key: :contact_id,
    primary_key: :id

  has_many :shared_users,
    through: :shared_contacts,
    source: :user
end
