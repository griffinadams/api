class RemoveColumnFromContact < ActiveRecord::Migration
  def up
    remove_column :contacts, :user_id_id
    add_column :contacts, :user_id, :integer
    add_index :contacts, [:email, :user_id], unique: true, name: 'email_user_index'
  end

  def down
    add_column :contacts, :user_id_id, :integer
    remove_column :contacts, :user_id
  end
end
