class CreateContactShare < ActiveRecord::Migration
  def change
    create_table :contact_shares do |t|
      t.references :contact
      t.references :user

      t.timestamps
    end

    add_index :contact_shares, :contact_id, name: 'contact_index'
    add_index :contact_shares, :user_id, name: 'share_owner_index'
  end
end
