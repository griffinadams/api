class AddIndexToContacts < ActiveRecord::Migration
  def change
    add_index :contacts, :user_id, name: 'owner_index'
  end
end
