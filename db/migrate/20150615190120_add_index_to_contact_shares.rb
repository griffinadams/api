class AddIndexToContactShares < ActiveRecord::Migration
  def change
    add_index :contact_shares, [:contact_id, :user_id], name: "contact_user_index", unique: true
  end
end
