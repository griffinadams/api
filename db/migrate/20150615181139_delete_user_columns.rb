class DeleteUserColumns < ActiveRecord::Migration
  def up
    remove_column :users, :name
    remove_column :users, :email
    add_column :users, :user_name, :string
  end

  def down
    add_column :users, :name, :string
    add_column :users, :email, :string
    remove_column :users, :user_name
  end
end
